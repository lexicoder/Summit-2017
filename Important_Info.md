## Hola!

This is _mandatory reading_ to make sure you take maximum advantage of this awesome week of GitLab fun.<br>
Check out the calendar invites and ask your questions **beforehand** in the #git-push-mexico channel on Slack.<br>
Understand that all activities are **optional**, but make sure to **RSVP yes/no**. When responding with **“maybe”** it will be recorded as a **"no"**.

#### Phone numbers

Save at least these two phone numbers in your phone in case you get kidnapped our get in any other trouble.<br>
And no, we don’t have the money to bail you out of jail.<br>
**Kirsten** +1 415 966 8495<br>
**Emily** +1 415 377 1506

#### Packing list

See a small [packing list](https://gitlab.com/summits/Summit-2017/blob/master/packing_list.md) as an _example_ of items to bring.<br>
Feel free to suggest other items in the #git-push-mexico channel on Slack, or as a merge request for that page.

#### GitLab swag

In Mexico we’re handing out some awesome, custom made swag! Leave extra room in your suitcase to bring these goodies home.

#### Arriving in Mexico

Get and Uber or cab at the airport and try to share rides with other GitLabbers as often as possible. Please expense this at end of trip. 


#### Significant Others

SOs are welcome to join in all meals and activities. Please **RSVP with +1** when your SO would like to participate. 

#### Expenses

Only transportation to and from the airport can be expensed. Food, bevreage, and lodging will alreday be covered by the company.


#### Activity info

**RSVP to the calendar invites** and be on time! We will leave without you if you're not there. Cal invites will have details of each event and what if anything you need to bring.<br>

#### Share photos & music

Let’s share all the pictures that we take during this week using the free **Bonfyre** app for [iOS](https://itunes.apple.com/us/app/bonfyre-app-free-private-photo/id463042005?mt=8) & [Android](https://play.google.com/store/apps/details?id=com.mobileapp.bonfyre&referrer=utm_source%3Dhomepage%26utm_medium%3Dweb%26utm_campaign%3Dbonfyre) and listen & add songs to our [Git Push Mexico](https://open.spotify.com/user/kideruiter/playlist/1eMznesracL0SK1LOrIc0N) on Spotify.

#### TL;DR

1. Pack wisely
1. RSVP
1. Consult Slack chanel for event updates and questions
1. Stay hydrated but don't drink the tap water. We will provide bottled water
1. Use sunscreen and bug spray (to protect from Zika)
1. Introduce yourself  to new faces
1. Call Kirsten or Emily when you get kidnapped
1. Have fun!
