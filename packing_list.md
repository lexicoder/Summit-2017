# Hints for Packing

Notes: Almost everything we have planned is a casual AKA you can wear, jeans/ shorts and a t-shirt. The one exception to this is our Casino night dinner. The theme for the evening is Casino Royal. Suggested attire- dress shirt and dresses.
The temp is expected to be 82F/ 28C.

### Nice to have Items
- Light rain jacket or poncho
- Swim suit or something you don't mind getting wet in- the hotel has a pool and a beach.
- Travel adapters & extention cords (label them with your name)
- Whatever you feel most confortable in.
- Something a bit dressier to wear to our casino night dinner
- Closed-toed, sport shoes will be needed for zip lining
- A hat
- Sandals or water shoes
- Sunglasses and possibly a sunglass strap- will be helpful for ziplining
- Sunscreen and bug spay- we will have some but always good to have more
- Any games you might like to play with the team on game night
    - Add games you are bringing to the spreadsheet so we don't bring multiples of the same game!
    - https://docs.google.com/a/gitlab.com/spreadsheets/d/1n0Fzef36iRji2pZRds_PAuKCCWi81k25urt_FfLMLc4/edit?usp=sharing


### Recommended travel documents:
-  GitLab invitation email
   - Including hotel address
-  Printed copies of passport and visas
-  Flight tickets
-  Passport
-  Visa