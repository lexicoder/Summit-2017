**Cancun Emergency Numbers**
- Emergencies: 066
- Fire Department: 52 (998) 884-1202 / 884-94-80
- Police Department: 52 (998) 884-1913 / 885-2277
- Red Cross: 52 (998) 84-1616 emergencies: 065
- AA (24 hours) (English):52 (998) 884-6909 / 8841271
- Harbor:52 (998) 880-1360, 1362, 1363

**Cancun Hospitals**
- Amerimed
(24 hr.) Plaza Las Americas, downtown
Telephone: 52 (998) 881-34-00


- Total Assit
(24hr. 5 Claveles St., downtown
Telephone: 52 (998) 884-10-92 & 52 (998) 884-8082


- Hospital Americano
(24hr.) 15 Viento St. Downtown
Telephone: 52 (998) 884-61-33


- AMAT hospital
(24hr) 13 Nader St., downtown
Telephone: 52 (998) 887-4422


- Hospiten
(24hr) Bonampak Ave., downtown
Telephone: 52 (998) 881-3700


- Galenia
(24hr) Banampak Ave., downtown
Telephone: 52 (998) 891-5200