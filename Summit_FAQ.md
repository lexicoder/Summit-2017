# Summit FAQ

1. What happens if I need to travel through the US to get to Mexico?
 - If you need a US Transit Visa (ESTA) and have an e-passport you can find more information [here](https://www.cbp.gov/travel/international-visitors/frequently-asked-questions-about-visa-waiver-program-vwp-and-electronic-system-travel). If you are not eligible to apply for an ESTA please contact People Ops to help you apply for a C-1 Transit Visa. 
 - Please make sure to apply on the [official US website](https://esta.cbp.dhs.gov/esta/) and pay the $14 charge. Other sites may charge more for the same application.  

1. How to I apply for a Visa to get to Mexico?
 - If you need a Visa to enter Mexico please contact People Ops to assist you with the application process. 
 - You may also want to consider applying for a US Visa as well to ensure that one of the visas come through in time for the Summit. 
 - To apply for a Mexican Visa you need to:
    - Schedule an appointment at the Mexican Embassy as soon as possible.
    - Gather all documentation from People Ops necessary. Please research what your the necessary documents are for your country as it may differ. 
        - Typical documents that have been needed so far: Visa Invitation Letter, Letter of Employment, GitLab Banking Summary Statements, Contract of Employment, etc. 
    - Once you have had your visa interview you should be notified within two business days of the status, but administrative delays are very possible. 
 - To apply for a US Visa to go to Mexico
    - Schedule an appointment at the US Embassy as soon as possible.
    - Fill out form [DS-160](https://ceac.state.gov/GenNIV/Default.aspx). Here are some helpful [instructions](https://drive.google.com/open?id=0B4eFM43gu7VPRlhzZDVkNzdZRG5JcWZCV25TTjY5TkFrSEpJ) on how to fill out the form. 
    - To apply for a nonimmigrant visa, you will have to pay the $160 non-refundable visa application fee at a designated branch. Please look to see where you can do this in your country.
    - If you are only traveling through the US, you can apply for a C-1 Visa. If you are planning on staying in the US for personal travel, you can apply for a B-2 Visa. If you have questions about which Visa type to apply for please ask People Ops. 

1. If my passport is near expiration, will GitLab pay for my renewal?
 - GitLab will assist with an visa related expenses, but not passport expenses. For more information, please ask People Ops. 

1. Please add to me.