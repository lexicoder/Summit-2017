# GitLab Summit Cancun 2017

### Who:

All GitLab team, and core team

### What:

We try to get together every 9 months or so to all get some face time together,
build community, and get some work done!

### Where:

+ [Cancun (CUN), Mexico](https://en.wikipedia.org/wiki/Canc%C3%BAn)

### When: 

+ January 10th - 17th 2017


### Overview

We're staying at the Barcelo Hotel (Ask Kirsten/Emily if you have questions about/for the hotel)

Please make sure to let Kirsten know (if you just joined) **ASAP** if:
1. you will or cannot stay the full week
1. you plan to bring your SO (and/or kids) or not
1. if there is any specific reason you cannot share a room (apart from bringing your SO)
1. 3 Random trivia items about yourself

Please understand that **everyone will share a room** with a team member with the exeption of bringing an SO or specific reason.
If you choose **not** to want to share you will have to pay the difference to get a private room; **~$250 personally (not expensable)**

## List of documents/resources

- [Important Info](https://gitlab.com/summits/Summit-2017/blob/master/Important_Info.md)
- [Summit FAQ](Summit_FAQ.md)
- [Travel Plans](https://docs.google.com/spreadsheets/d/12MqT7aBHHGPes45Q9jTNyYRTTx9or7FPNTEl3HWJwoE/edit#gid=1198457175)  (commentable with GitLab login)
- [Packing List](https://gitlab.com/summits/Summit-2017/blob/master/packing_list.md) 
- [Expenses](https://gitlab.com/summits/Summit-2017/blob/master/Mexico_Expenses.md) 
- [Egencia](www.egencia.com) can be used to book your flight (no cc needed) or feel free to use any other means and expense your flight (Expensify for employees / your monthly bill for contractors)
- [Getting Work Done](https://gitlab.com/summits/Summit-2017/blob/master/Getting_Work_Done.md)

### External

- [CDC Health information](http://wwwnc.cdc.gov/travel/destinations/traveler/none/mexico)
- [CDC info on the Zika virus](http://wwwnc.cdc.gov/travel/notices/alert/zika-virus-mexico)
    - Specific info for women who are (trying to get) pregnant: [here](http://www.cdc.gov/zika/pregnancy/thinking-about-pregnancy.html)
- [Visa info]( https://mexico.visahq.com/) > get in touch with Brittany when you need a visa

### Questions? 

+ Create an [issue](https://gitlab.com/summits/Summit-2017/issues/new) and cc both @kirstenabma and @EmilyKyle 
