## Play Hard, Work Harder...

* We have reserved a giant meeting room (fits 250) at the hotel everyday (Tues 10 - Mon 16). The default layout of the room will be round tables that seat 10, and one screen and projector that are portable.  
* This room is free for anyone to use anytime even when we are out on excursions. 
* If you would like to plan a specific meeting with a group in the room please add your name to the [google doc](https://docs.google.com/a/gitlab.com/spreadsheets/d/1rJy2q3BoyR4XKbXP7YYbzj2ZA7fbZU9O-QTcHcpOgic/edit?usp=sharing) with the time slot(s) you wish to reserve along with how many you expect to attend the meeting and your preferred seating arrangement in parenthesis. We will contact you to confirm your seating and tech needs.
   * Multiple groups can reserve the same time slot.
   * You will need to send out your own cal invite to your group for your meeting.